Overview
================================================================================

The Social Speech module allows users to interact with your site content by
highlighting, clicking, and sharing individual sentences.

End User Features
* On hover, individual sentences are highlighted
* On click, a modal dialog is displayed to the user
** Modal dialog may contain:
*** Facebook and Twitter share links
**** Shared links are sentence specific, and will take a visitor to the shared
sentence
*** Custom content (specified by CSS selector)


Social Speech also contains a submodule called Social Speech Webform, which
allows you to embed a webform in the modal dialog and submit that form via AJAX.

Developer Features
* Social Speech provides two hook, which permit developers to modify the editing
and display of Social Speech nodes:
** hook_social_speech_presave()
** hook_social_speech_view()
* Social speech also provides "jQuery hooks," which allow developers to respond
to jQuery events.

This module was initially built by the White House for the State of the Union
Address in 2013. Please see whitehouse.gov/sotu2013 for a demonstration.


Installation
===============================================================================

Please see INSTALL.txt.


Usage
================================================================================


Text Parsing
--------------------------------------------------------------------------------

Social Speech textarea fields are parsed via the following alogrithm:
* Field value is converted into an Simple HTML DOM html object.
* Social Speech iterates over each <p> tag in HTML markup.
* The content string within each <p> tag is passed through a regex, which parses
strings where it finds a period (".").
** The regex will ignore popular abbreviations that use periods. E.g., "Mr.".
   The list of abbreviations can be modified at admin/config/social_speech.
