<?php
/**
 * @file
 * Customize social speech field.
 *
 * Available variables:
 * - $speech: The markup for the field using social_speech formatter.
 */
?>
<div class="social-speech-node">
  <?php print $speech; ?>
</div>
