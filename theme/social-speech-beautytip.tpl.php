<?php
/**
 * @file
 * This template allows for the theming of social links.
 *
 * Social links are the default content for a beautytip modal.
 *
 * The accompanying jQuery expects the following elements to exist:
 *   - $('#social-speech-social-links-wrapper')
 *   - $('a.social-speech-share-url')
 *   - $('a.twitter-share-button')
 *   - $('a.facebook-share-button')
 */
?>
<div id="social-speech-social-links-wrapper">
  <div class="social-speech-social-links-inner">
    <div class="social-speech-share-url-wrapper">
      <a class="social-speech-share-url" href="#">[placeholder]</a>
    </div>
    <div class="soc-links">
      <a href="#" class="social-speech-share-button facebook-share-button" target="_blank" >Share on Facebook</a>
      <a href="#" class="social-speech-share-button twitter-share-button"  target="_blank">Tweet</a>
    </div>
  </div>
</div>
