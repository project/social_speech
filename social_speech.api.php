<?php

/**
 * @file
 * API Documentation file for Social Speech module.
 */

/**
 * Enables social speech node to be modified before it is saved.
 *
 * This is triggered before save a social_speech enabled node. You could also
 * use hook_nodeapi() to modify the node object presave, but it would not
 * provide you with $form or $settings variables.
 *
 * @param array $form
 *   The node form.
 *
 * @param array $form_state
 *   The node form state.
 *
 * @param array $settings
 *   The social speech settings for this form.
 */
function hook_social_speech_presave($form, &$form_state, &$settings) {
  // Add new data to social speech settings.
  $settings['example_setting'] = $form_state['values']['example_form_field'];
}

/**
 * Enables social speech node to be modified before it is viewed.
 *
 * @param array $element
 *   The field element that is being displayed using social_speech formatter.
 *
 * @param string $output
 *   The field output markup for the aforementioned field.
 *
 * @param object $node
 *   The node being rendered.
 *
 * @param array $settings
 *   The Social Speech settings for this node.
 */
function hook_webform_social_speech_view(&$element, &$output, &$node, &$settings) {

  // Add custom JS when a social speech node is being displayed.
  // @see social_speech/js/example.js.
  $module_path = drupal_get_path('module', 'example');
  drupal_add_js($module_path . '/js/example.js');

  // Directly modify the social_speech JS settings.
  $json_data['social_speech']['bt_width'] = '100px';
  drupal_add_js($json_data, 'setting');

}
