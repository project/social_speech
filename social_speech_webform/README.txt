Description
-------------------------------------------------------------------------------
This module extends the social_speech module to make it more readily compatible
with the webform module.

Notable features include:
 * Adds ability to specify "confirmation message" CSS selector for webforms
   displayed in the Social Speech beauty tip.
 * Adds default webform "confirmation message" to Drupal settings object for use
   via javascript.
 * Adds CSS and JS for webforms displayed in the Social Speech beauty tip.
