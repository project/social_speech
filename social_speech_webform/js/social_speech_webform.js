/**
 * @file
 * Contains javascript methods for social_speech_webform module.
 */

Drupal.behaviors.socialSpeechWebform = function (context) {

  var settings = Drupal.settings.social_speech;

  /**
   * Fires after beautytip has been rendered.
   */
  $(document).bind('socialSpeechBTPostShow', function(event, clicked_segment) {

      // Grab id of clicked sentence.
      var sentence_id = clicked_segment.attr('id');
      set_sentence_id(sentence_id);

      // Bind form submit handlers to this new, cloned form.
      bind_form_submit(sentence_id);

      // Fix strange label click behavior.
      fixLabelBug();

      $('.field-items h2 a').removeAttr('href');

      // Prevent browsers from performing clientside form validation.
      $('.bt-wrapper form').attr('novalidate' , '');

      // Compact forms integration.
      if (typeof Drupal.settings.compactForms != 'undefined') {
        Drupal.behaviors.compactForms(context);
      }

  });

  /**
   * Sets hidden inputs to submit clicked sentence information.
   *
   */
  function set_sentence_id(id) {
    var sentenceid_component = settings.webform_sentenceid_component;
    $(sentenceid_component).val(id);

    var sentence_component = settings.webform_sentence_component;
    var sentence_content = $('#' + id).text();
    $(sentence_component).val(sentence_content);
  }

  /**
   * POST form via AJAX, respond to submission.
   */
  function bind_form_submit(sentence_id) {
    var form = jq183('.bt-wrapper form');
    var postAction = form.attr('action');

    form.submit(function() {

      // POST form values.
      jq183.ajax({
        type: "POST",
        url: postAction,
        data: form.serialize(),
        beforeSend: function (jqXHR, settings) {
          // Allow other modules to react to this AJAX form submission.
          $.event.trigger('socialSpeechWebformAJAXSubmit', [form]);

          // Display social links content.
          displayWebformConfirmation();

        },
        error: function(jqXHR, textStatus, errorThrown) {
          $.event.trigger('socialSpeechWebformAJAXError', [form]);
        },
        success: function(data, textStatus, jqXHR) {
          $.event.trigger('socialSpeechWebformAJAXSuccess', [form]);
        }
      });

      return false;
    });
  }

 /**
  * Fix strange behavior that causes clicking on labels to close tooltip.
  */
  function fixLabelBug(){
    // Move the input outside of the label element.
    $('.bt-wrapper form label input').each(function() {
      $(this).prependTo($(this).parent('label').parent());
    });
    // Unbind click behaviors.
    $('.bt-wrapper form label').click(function(){
      return false;
    });
  }

  /**
   * Generate social links content.
   */
  function displayWebformConfirmation() {
    var displayMarkup = "";
    var confirmation_text;

    // Check to see if a selector has been specificed for the webform
    // confirmation message.
    if (typeof settings.webform_confirmation != 'undefined') {
      confirmation_text = $(settings.webform_confirmation);
    }
    else {
      confirmation_text = settings.webform_confirmation_default;
      displayMarkup += confirmation_text;
    }

    // Append social links.
    if (typeof settings.webform_confirmation_append_social != 'undefined' && settings.webform_confirmation_append_social) {
       displayMarkup += settings.social_links;
    }

    $('.bt-content').html(displayMarkup);

  }
};
