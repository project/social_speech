/**
 * @file
 * Extend the Social Speech Webform module's JS.
 */

Drupal.behaviors.example = function (context) {
  /**
   * Fires after beautytip webform 'submit' button has been clicked, but just
   * before the AJAX request is made.
   */
  $(document).bind('socialSpeechWebformAJAXSubmit', function(event, form) {
    alert(form.attr('id') + ' was submitted via AJAX!');
  });

};
