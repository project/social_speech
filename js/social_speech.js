/**
 * @file
 * Contains javascript methods for social_speech module.
 */
Drupal.behaviors.socialSpeech = function (context) {

  // Add .exists() function to jQuery.
  jQuery.fn.exists = function(){return this.length > 0;};

  // Adds outerHTML function to jQuery.
  jQuery.fn.outerHTML = function(s) {
      return s ? this.before(s).remove() : jQuery("<p>").append(this.eq(0).clone()).html();
  };

  // Add .getUrlVars() function to jQuery.
  // @see http://jquery-howto.blogspot.com/2009/09/get-url-parameters-values-with-jquery.html
  $.extend({
    getUrlVars: function(){
      var vars = [], hash;
      var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
      for(var i = 0; i < hashes.length; i++)
      {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
      }
      return vars;
    },
    getUrlVar: function(name){
      return $.getUrlVars()[name];
    }
  });

  // If the user arrives with a sentence parameter in the URL.
  if (typeof $.getUrlVar('sss_id') !== 'undefined') {
    // Extract sentence ID from URL parameters.
    var sentence_id = $.getUrlVar('sss_id');
    var sentence = $('#' + sentence_id);

    // Highlight sentence.
    sentence.addClass('active');

    // Scroll to sentence.
    $('html, body').animate({
       scrollTop: sentence.offset().top
    }, 2000);
  }

  /**
   * Define pop-up form behavior.
   */
  var bt_settings = {
    trigger: ['click'],
    closeWhenOthersOpen: true,
    fill: Drupal.settings.social_speech.bt_fill,
    strokeWidth: Drupal.settings.social_speech.bt_strokeWidth,
    spikeGirth: Drupal.settings.social_speech.bt_spikeGirth,
    width: Drupal.settings.social_speech.bt_width,
    padding: Drupal.settings.social_speech.bt_padding,
    positions: ['bottom'],
    preBuild: function() {
      // Updates social links markup and value in Drupal.settings.
      socialLinks($(this));
    },
    preShow: function() {
      $(this).addClass('clicked-segment');
    },
    postShow: function() {
      // Allow others scripts to react to this beauty tip being shown.
      $.event.trigger('socialSpeechBTPostShow', [$(this)]);
    },
    postHide: function() {
      $(this).removeClass('clicked-segment');
    }
  };

  // If user has set custom beautytip content, display it.
  if (typeof Drupal.settings.social_speech.bt_selector != 'undefined') {
    bt_settings.contentSelector = $(Drupal.settings.social_speech.bt_selector);
    $(Drupal.settings.social_speech.bt_selector).hide();
  }
  // If no custom beautytip is specified, set default beautytip content to
  // social links.
  else {
    // We need to create a hidden, placeholder element to populate with social
    // link markup.
    $('body').append('<div id="ss-beauty-tip-content" />');
    bt_settings.contentSelector = $('#ss-beauty-tip-content');
    $('#ss-beauty-tip-content').html(Drupal.settings.social_speech.social_links);
    $('#ss-beauty-tip-content').hide();
  }

  // Enable BeautyTip behavior.
  $('.social-speech-segment').bt(bt_settings);

  /**
   * Fires after beautytip has been rendered.
   */
  $(document).bind('socialSpeechBTPostShow', function(event, clicked_segment) {
  });

  /**
   * Generate social links content.
   */
  function socialLinks(clicked_segment){
    var social_links = $(Drupal.settings.social_speech.social_links);
    var sentence_id = clicked_segment.attr('id');

    // Grab share URL.
    var share_url = Drupal.settings.social_speech.share_url + '?sss_id=' + sentence_id;
    social_links.find('a.social-speech-share-url').attr('href', share_url);
    social_links.find('a.social-speech-share-url').text(share_url);

    // Build twitter link.
    var tweet_link_params = {
      url: share_url,
      text: Drupal.settings.social_speech.twitter_text
    };
    var tweet_param_string = jQuery.param(tweet_link_params);
    var tweet_link_href = 'https://twitter.com/intent/tweet?' + tweet_param_string;
    social_links.find('a.twitter-share-button').attr('href', tweet_link_href);

    // Build facebook link.
    var fb_link_params = {
      s : 100,
      'p[title]' : Drupal.settings.social_speech.facebook_text,
      'p[summary]' : clicked_segment.text(),
      'p[url]': share_url,
      'p[images]': Drupal.settings.social_speech.share_img
    };

    var fb_param_string = jQuery.param(fb_link_params);
    var fb_link_href = 'https://www.facebook.com/sharer/sharer.php?' + fb_param_string;
    social_links.find('a.facebook-share-button').attr('href', fb_link_href);

    // Save modified markup to Drupal settings.
    Drupal.settings.social_speech.social_links = social_links.outerHTML();
    // Write modified markup to DOM element.
    $('#ss-beauty-tip-content').html(Drupal.settings.social_speech.social_links);
  }

};
