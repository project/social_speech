/**
 * @file
 * Extend the Social Speech module's JS.
 */

Drupal.behaviors.example = function (context) {

  /**
   * Fires after beautytip has been rendered.
   */
  $(document).bind('socialSpeechBTPostShow', function(event, clicked_segment) {
    alert(clicked_segment.attr('class') + ' was clicked!');
  });

};
