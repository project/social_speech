<?php

/**
 * @file
 * Administrative form and page callbacks for the social_speech module.
 */

/**
 * Form callback for admin/settings/social-speech.
 */
function social_speech_admin_form() {
  $form = array();

  $form['social_speech_abbreviations'] = array(
    '#type' => 'textarea',
    '#title' => t('Ignored Abbreviations'),
    '#description' => t('These abbreviations will be ignored by the REGEX that parses sentences. Abbreviations must be separated by a line break and end with a period.'),
    '#default_value' => social_speech_default_text('social_speech_abbreviations', ''),
  );

  return system_settings_form($form);
}


/**
 * Validates social_speech_admin_form.
 */
function social_speech_admin_form_validate($form, &$form_state) {
  $values = $form_state['values'];

  $abbreviations_array = explode("\n", $values['social_speech_abbreviations']);
  $regex = '/^([A-Za-z0-9])+(\.){1}$/';

  foreach ($abbreviations_array as $abbreviation) {
    $abbreviation = trim($abbreviation);
    // Check that abbreviations are in the correct format.
    if (!preg_match($regex, $abbreviation, $matches)) {
      form_set_error('social_speech_abbreviations', t('The abbreviations field must consist of only alphanumeric characters, followed by a period.'));
    }
  }
}
